//
//  Configuration.swift
//  RassedySDK
//
//  Created by mac on 22/05/2022.
//

import Foundation


public class Configuration{
    public static func start(nav: UINavigationController?){
        let bundle = Bundle(identifier: "com.paymob.RaseedySDK")
        
        let storyboard = UIStoryboard(name: "RaseedyEntryPoint", bundle: bundle)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "RaseedyEntryPointVC")
        
        nav?.pushViewController(vc, animated: true)
    }
}
