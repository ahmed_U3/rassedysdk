#
# Be sure to run `pod lib lint RassedySDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RassedySDK'
  
  #is the new tag every update that you will push on repo
  s.version          = '0.0.29'
  
  #must be a valiable content
  s.summary          = 'Rassedy framework.'

  # must add avaliable description
  s.description      = <<-DESC
  Rassedy App to Use it as a framework
                       DESC

  s.homepage         = 'https://gitlab.com/ahmed_U3/rassedysdk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'ahmed yousry' => 'ahmedadly@paymob.com' }
  s.source           = { :git => 'https://gitlab.com/ahmed_U3/rassedysdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  #minimum version that you want to run the SDK
  s.ios.deployment_target = '10.0'
  
  #swift version that you use
  s.swift_version = '4.0'
  
  #you can make mulible sources like
  #swift files
  s.source_files = 'RassedySDK/Classes/Configuration.swift'
  
  #when you need to add a framework
  s.ios.vendored_frameworks = 'RassedySDK/Classes/RaseedySDK.xcframework'
  
  #target configurations for simulator and device
  s.pod_target_xcconfig = { 'VALID_ARCHS' => 'arm64 arm64e armv7 armv7s x86_64' }
  
  
  #if you build framework from projects that contains Pods you must add these pods as a de
  s.dependency 'Alamofire', '~> 4.7.0'
  s.dependency 'AlamofireObjectMapper', '~> 5.0'
  s.dependency 'RealmSwift', '~> 3.20.0'
  s.dependency 'RxSwift'
  s.dependency 'RxCocoa'
  s.dependency 'DrawerController'
  s.dependency 'RxKeyboard'
  s.dependency 'SDWebImage'
  s.dependency 'RxReachability'
  s.dependency 'QRCodeReader.swift'
  s.dependency 'Kingfisher'
  s.dependency 'Firebase'
  s.dependency 'Firebase/Core'
  s.dependency 'Firebase/Messaging'
end
