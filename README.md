# RassedySDK

[![CI Status](https://img.shields.io/travis/ahmed yousri/RassedySDK.svg?style=flat)](https://travis-ci.org/ahmed yousri/RassedySDK)
[![Version](https://img.shields.io/cocoapods/v/RassedySDK.svg?style=flat)](https://cocoapods.org/pods/RassedySDK)
[![License](https://img.shields.io/cocoapods/l/RassedySDK.svg?style=flat)](https://cocoapods.org/pods/RassedySDK)
[![Platform](https://img.shields.io/cocoapods/p/RassedySDK.svg?style=flat)](https://cocoapods.org/pods/RassedySDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RassedySDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RassedySDK'
```

## Author

ahmed yousri, ahmedyousrifci@gmail.com

## License

RassedySDK is available under the MIT license. See the LICENSE file for more info.
